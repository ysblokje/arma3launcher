#ifndef __STEAMQUERIES_HPP__
#define __STEAMQUERIES_HPP__
#include <functional>
#include <vector>
#include <string>
#include <set>
#include <cstdint>
#include <memory>


class gameserveritem_t;
class ISteamMatchmakingServers;
class ISteamApps;
class SteamUGCQueryCompleted_t;

namespace arma3 {

    // For persistance a settings class.

    struct Settings {
        Settings();
        void loadSettings();
        void saveSettings();
        // THIS IS WITHOUT THE MODS!
        std::string generateParametersString();

        struct Parameters {
            bool enable_hyperthreading=false;
            bool no_splash=false;
            bool skip_intro=false;
            bool windowed=false;
            int  cores=-1;
            int  threads=-1;
            bool vblank=false;
            std::string additional_parameters;
        } parameters;

        enum NvidiaOptions {
            nv_none=0,
            nv_primusrun,
            nv_optirun,
        } nvidiaoptions;

        std::string mod_path;
        std::string game_path;

        std::set<std::string> enabled_mods;
    };




    class SteamConnection;

    typedef std::vector<std::string> StringVector;

    /*
     * Split a string using a delimitter
     */
    inline StringVector &split(const std::string &s, char delim, StringVector &elems) {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }

    /*
     * Properties that a can have, add more when needed.
     */
    struct Mod {
        Mod() {}
        Mod(const std::string &name, const std::string &location) : name(name), location(location) {}
        std::string name;
        std::string location;
        bool isworkshopitem=true;
        bool checked=false;
        std::string hash;
        std::string steamid;
        std::string signature;
    };
    typedef std::vector<Mod> Mods;


    struct KV {
        std::string k;
        std::string v;
    };


    /*
     * Some detailed properties a running server can have.
     */
    class ServerDetails {
    public :
        int dlccount=0;
        int modcount=0;
        bool dlc_kart=false;
        bool dlc_mark=false;
        bool dlc_heli=false;
        bool dlc_expa=false;

        std::string kart_hash;
        std::string mark_hash;
        std::string heli_hash;
        std::string expa_hash;

        Mods mods;
    };

    /*
     * when asking for a list of mods the response
     * will come through a callback.
     */
    typedef std::function<void(Mods&)> ModsReceivedCallback;

    /*
     * When  asking for the detailedproperties of a server
     * these callbacks can be called depending on the
     * situation.
     */
    struct ISteamMatchmakingRulesResponseCallback {
        std::function<void()> ServerResponded_cb;
        std::function<void()> ServerFailedToRespond_cb;
        std::function<void(ServerDetails &)> RefreshComplete_cb;
    };

    /*
     * The base description of a server.
     */
    class GameServer {
        friend class SteamConnection;
        friend class ServerListResponse;
    public:

        ~GameServer();
        void getServerDetails(ISteamMatchmakingRulesResponseCallback&);
        StringVector getTags();
        std::string getVersion();
        std::string getGameType();
        std::string getName();
        bool usesBattleEye();
        int getPing();
        std::string getIpAddress();
        uint16_t getPort();

    protected:
        GameServer( gameserveritem_t*);
        class GameServerImpl;
        GameServerImpl *p_impl;
    };
    typedef std::shared_ptr<GameServer> GameServer_ptr;


    /*
     * When asking for a list of servers
     * the response will coming the form of a callback that will be called
     */
    struct ISteamMatchmakingServerListResponseCallback {
        std::function<void(GameServer_ptr)> ServerResponded_cb;

        // std::function<void(HServerListRequest, int)> ServerFailedToRespond_cb;
        // std::function<void(HServerListRequest, EMatchMakingServerResponse)> RefreshComplete_cb;
    };


    /*
     * A class to get a serverlist
     * We need additional "stuff" like filters in here.
     */
    class ServerListResponse {
        friend class SteamConnection;
    public :
        enum ServerListType {
            serverlist_internet=0,
            serverlist_favorites,
            serverlist_friends,
            serverlist_history,
            serverlist_lan,
        };

        struct Filter {
            Mods mods;
            std::string version;
            bool dontshowfull=false;
            bool showbattleye=false;
        };
        void getServerList(ServerListType slt, const Filter&);
        void getServerList(ServerListType slt);
        void stopQuery();
        ~ServerListResponse();
    protected:
        ServerListResponse(ISteamMatchmakingServers *servers, ISteamMatchmakingServerListResponseCallback& responsecallback);
    private:
        class ServerListResponse_impl;
        ServerListResponse_impl *p_impl;

    };
    typedef std::shared_ptr<ServerListResponse> ServerListResponse_ptr;

    /*
     * Find installed mods
     * And use selected mods to get a commandline going.
     */
    class ModsQuery {
    public :
        ModsQuery(const std::string &modspath);
        ~ModsQuery();
        void setChecked(size_t idx, bool checked);
        std::string getCommand();
        void getMods(ModsReceivedCallback &mrc);
        const Mods& getFoundMods();

        class ModsQueryImpl;
        ModsQueryImpl *p_impl;
    };
    typedef std::shared_ptr<ModsQuery> ModsQuery_ptr;

    /*
     * The connection to a running steam instance.
     */
    class SteamConnection {
    public :
        SteamConnection();
        ~SteamConnection();
        void init(std::function<void(bool)> initDoneAndSteamRunning_cb);
        ServerListResponse_ptr getServerListResponeObject(ISteamMatchmakingServerListResponseCallback &callbacks);
        ModsQuery_ptr getInstalledMods(const std::string &modpath);
        void tickOnce();

    private :
        bool init_success =false;
        bool running = false;
        ISteamApps *apps=nullptr;
        ISteamMatchmakingServers *servers=nullptr;
    };



}//namespace arma3

#endif //__STEAMQUERIES_HPP__
