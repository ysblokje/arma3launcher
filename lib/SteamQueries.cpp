#include <sstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <steam/steam_api.h>
#include <steam/matchmakingtypes.h>
#include <steam/isteamnetworking.h>
#include "SteamQueries.hpp"


namespace arma3 {

    namespace fs = boost::filesystem;
    namespace pt = boost::property_tree;

    inline fs::path getConfigLocation() {
        fs::path retval;
        const char* home = getenv("HOME");
        if(home) {
            retval = home;
        } else {
            // some extra env. vars. perhaps?
            retval = fs::temp_directory_path();
        }
        retval/=".config/Arma3Launcher";
        return retval;
    }

#define CONFIGFILE "setup.conf"


#pragma packed(1)
    struct ServerResponseV2Header {
        uint8 version;
        uint8 flags;
        uint8 dlcflags;
        uint8 reserved;
        uint8 difficulty;
        uint8 difficulty2;
    };
#pragma()
    union QueryResponse {
        ServerResponseV2Header header;
        char rawdata[1400];
    };


#define FLAG_KART  0x1
#define FLAG_MARK  0x2
#define FLAG_HELI  0x4
#define FLAG_EXPA  0x8



    typedef std::vector<char> char_vector;

    class ServerResponseV2 : public ServerDetails {

    public :
        ServerResponseV2(const char* data, size_t detected_size) {

            std::cerr << "total size :" << detected_size << "\n";
            QueryResponse *qr=((QueryResponse*)data);
            if(qr->header.version==2) {
                //memcpy(qr.rawdata, data, sizeof(qr.rawdata));
                if(qr->header.dlcflags>31) {
                    std::cerr << "UNKNOWN DLC!\n";
                }
                if((qr->header.dlcflags & FLAG_KART)!=0) {
                    dlccount++;
                    dlc_kart = true;
                }
                if((qr->header.dlcflags & FLAG_MARK)!=0) {
                    dlccount++;
                    dlc_mark = true;
                }

                if((qr->header.dlcflags & FLAG_HELI)!=0) {
                    dlccount++;
                    dlc_heli = true;
                }
                if((qr->header.dlcflags & FLAG_EXPA)!=0) {
                    dlccount++;
                    dlc_expa = true;
                }

                char *hash_offset = qr->rawdata+sizeof(ServerResponseV2Header);
                if(dlc_kart) {
                    kart_hash.assign(hash_offset, hash_offset+4);
                    hash_offset+=4;
                }

                if(dlc_mark) {
                    mark_hash.assign(hash_offset, hash_offset+4);
                    hash_offset+=4;
                }

                if(dlc_heli) {
                    heli_hash.assign(hash_offset, hash_offset+4);
                    hash_offset+=4;
                }
                if(dlc_expa) {
                    expa_hash.assign(hash_offset, hash_offset+4);
                    hash_offset+=4;
                }
                modcount = hash_offset[0];
                hash_offset++;
                int _modcount = modcount;
                std::cerr << "MODS " << _modcount << "\n";
                for(int i(0); i<modcount && hash_offset< data+detected_size; i++) {
                    Mod m;
                    m.hash.assign(hash_offset, hash_offset+4);
                    hash_offset+=4;
                    uint8 idlength=hash_offset[0];
                    hash_offset++;
                    if(idlength>1)
                        m.steamid.assign(hash_offset, hash_offset+idlength);
                    hash_offset+=idlength;
                    uint8 namelenght=hash_offset[0];
                    hash_offset++;
                    m.name.assign(hash_offset, hash_offset+namelenght);
                    hash_offset+=namelenght;
                    mods.push_back(m);
                }
            }
        }
    };

    struct kv {
        kv(uint8 k, uint8 v) : k(k), v(v) {}
        uint8 k;
        uint8 v;
    };


    std::vector<kv> translation= { kv(0x01, 0x01), kv(0x02, 0x00), kv(0x03,0xff)};


    static void translate(const char* in, char_vector &out) {
        auto it_end=translation.end();
        size_t j(0);
        int i(0);
        for(; in[i]!=0; i++, j++) {
            if(in[i]==0x01) {
                auto it=translation.begin();
                while(it!=it_end) {
                    if((*it).k==in[i+1]) {
                        out.push_back(it->v);
                        i++;
                        it = it_end;
                    } else
                        it++;
                }
            } else
                out.push_back(in[i]);
        }

    }


    static void unEscapeV2Protocol(std::vector<KV> keyvalues , ServerDetails**out) {
        size_t i;
        bool chunked =keyvalues.size()>1;

        char_vector dest;
        bool first=true;
        for(auto chunk(keyvalues.begin()); chunk!=keyvalues.end(); chunk++) {
            if(first) {
                translate(chunk->v.c_str(), dest);
                first = false;
            } else {
                char_vector tmp;
                translate(chunk->v.c_str(), tmp);
                dest.insert(dest.end(),tmp.begin(), tmp.end());
            }
        }

        ServerResponseV2 *retval = new ServerResponseV2(dest.data(), dest.size());

        *out = retval;

    }



    class GameServer::GameServerImpl : public ISteamMatchmakingRulesResponse {
    public :
        GameServerImpl(gameserveritem_t *gsi) {
            servers=SteamMatchmakingServers();
            processItem(gsi);
        }

        void processItem(gameserveritem_t *details) {
            ping = details->m_nPing;
            name = details->GetName();
            split(details->m_szGameTags, ',', tags);
            for(auto it(tags.begin()); it!=tags.end(); it++) {
                std::string &val(*it);
                switch(val[0]) {
                    case 'r':
                        version = val.substr(1);
                        break;
                    case 'b':
                        battleeye = val[1]=='t';
                        break;
                    case 't':
                        gametype = val.substr(1);
                        break;
                }
            }

            std::string l_address=details->m_NetAdr.GetConnectionAddressString();
            std::string::size_type pos = l_address.find(":");
            if(pos != std::string::npos) {
                address =  l_address.substr(0, pos);
                port = details->m_NetAdr.GetConnectionPort();
            }
            query_port = details->m_NetAdr.GetQueryPort();
            addr = details->m_NetAdr.GetIP();
        }

        void getServerDetails(ISteamMatchmakingRulesResponseCallback &cb) {
            kv.clear();
            responsecallback = cb;
            std::cerr << std::hex << addr << " " << std::dec << port << "\n";
            hrequest = servers->ServerRules(addr, query_port, this);
        }
        void RulesResponded( const char *pchRule, const char *pchValue ) {
            KV _kv;
            _kv.k=pchRule;
            _kv.v=pchValue;
            kv.push_back(_kv);
        }
        void RulesFailedToRespond() {
            if(responsecallback.ServerFailedToRespond_cb)
                responsecallback.ServerFailedToRespond_cb();
        }

        void RulesRefreshComplete() {
            if(!kv.empty()) {
                if(kv[0].k[0]>=' ') {
                    sr = new ServerDetails;
                    for(auto it=kv.begin(); it!=kv.end(); it++) {
                        std::cout << it->k << ":" << it->v << "\n";

                        std::vector<std::string> elem;
                        std::string values(it->v);
                        split(values, ';', elem);
                        if(it->k.size()>=4)
                            if(it->k.substr(0,4)=="mods") {

                                for(auto it2=elem.begin(); it2!=elem.end(); it2++) {
                                    Mod m;
                                    m.name = *it2;
                                    it2++;
                                    m.hash = *it2;
                                    sr->mods.push_back(m);
                                }
                            }
                    }
                } else {
                    std::cerr << "packets" << kv.size() << "\n";
                    unEscapeV2Protocol(kv, &sr);
                }

                if(sr!=nullptr)
                    if(responsecallback.RefreshComplete_cb)
                        responsecallback.RefreshComplete_cb(*sr);
            }
            kv.clear();
        }


        StringVector tags;
        std::string version;
        std::string gametype;
        std::string name;
        bool battleeye=false;
        int ping=-1;
        std::string address;


        std::vector<KV> kv;
        HServerQuery hrequest;
        ISteamMatchmakingServers* servers;
        uint32 addr;
        uint16 port;
        uint16 query_port;
        ServerDetails* sr=nullptr;
        ISteamMatchmakingRulesResponseCallback responsecallback;
    };


    GameServer::GameServer(gameserveritem_t *gsi) {
        p_impl = new GameServerImpl(gsi);
    }

    StringVector GameServer::getTags() {
        return p_impl->tags;
    }
    std::string GameServer::getVersion() {
        return p_impl->version;
    }
    std::string GameServer::getGameType() {
        return p_impl->gametype;
    }
    std::string GameServer::getName() {
        return p_impl->name;
    }
    bool GameServer::usesBattleEye() {
        return p_impl->battleeye;
    }
    int GameServer::getPing() {
        return p_impl->ping;
    }
    std::string GameServer::getIpAddress() {
        return p_impl->address;
    }
    uint16_t GameServer::getPort() {
        return p_impl->port;
    }

    GameServer::~GameServer() {
        std::cerr << this << " destroyed\n";
        delete p_impl;
    }

    void GameServer::getServerDetails(ISteamMatchmakingRulesResponseCallback&cb) {
        p_impl->getServerDetails(cb);
    }


    class ServerListResponse::ServerListResponse_impl : public ISteamMatchmakingServerListResponse  {
    public :
        ServerListResponse_impl(ISteamMatchmakingServers *servers, ISteamMatchmakingServerListResponseCallback &responsecallback) : servers(servers), responsecallback(responsecallback) {}

        void ServerResponded( HServerListRequest hRequest, int iServer ) {
            if(responsecallback.ServerResponded_cb) {
                gameserveritem_t *pServer = SteamMatchmakingServers()->GetServerDetails( hRequest, iServer );
                if ( pServer ) {
                    // Filter out servers that don't match our appid here (might get these in LAN calls s
                    if ( pServer->m_nAppID == SteamUtils()->GetAppID() ) {
                        gameserveritem_t *details = servers->GetServerDetails( hRequest,  iServer);
                        GameServer_ptr gs(new GameServer(details));
                        responsecallback.ServerResponded_cb(gs);
                    }
                }
            }
        }

        void ServerFailedToRespond( HServerListRequest hRequest, int iServer ) {
            //  if(responsecallback->ServerFailedToRespond_cb)
            //    responsecallback->ServerFailedToRespond_cb(hRequest, iServer);
        }

        void RefreshComplete( HServerListRequest hRequest, EMatchMakingServerResponse response ) {
            // if(responsecallback->RefreshComplete_cb)
            //     responsecallback->RefreshComplete_cb(hRequest, response);
        }

        void getServerList(ServerListType slt, const Filter &f) {
            filters.clear();
            std::string tags;
            if(!f.version.empty())
                tags = (std::string("r") + f.version);

            if(!tags.empty())
                tags+=",";

            if(f.showbattleye) {
                tags+= "bt";
            } else {
                tags+="bf";
            }


            if(!tags.empty()) {
                MatchMakingKeyValuePair_t p("gametagsand", tags.c_str());

                filters.push_back(p);
            }

            getServerList(slt);
        }

        void getServerList(ServerListType slt) {
            MatchMakingKeyValuePair_t *pFilter = &(filters[0]);
            auto id = SteamUtils()->GetAppID();
            switch(slt) {
                case serverlist_favorites:
                    hrequest  = servers->RequestFavoritesServerList(id, &pFilter, filters.size(), this);

                    break;
                case serverlist_friends:
                    hrequest  = servers->RequestFriendsServerList(id, &pFilter, filters.size(), this);

                    break;
                case serverlist_history:
                    hrequest  = servers->RequestHistoryServerList(id, &pFilter, filters.size(), this);

                    break;
                case serverlist_lan:
                    hrequest  = servers->RequestLANServerList(id, this);
                    break;
                case serverlist_internet:
                default :
                    hrequest  = servers->RequestInternetServerList(id, &pFilter, filters.size(), this);
                    break;
            }
        }

        void stopQuery() {
            servers->CancelQuery(hrequest);
        }
    private :

        ISteamMatchmakingServers* servers;
        ISteamMatchmakingServerListResponseCallback &responsecallback;
        HServerListRequest hrequest;
        std::vector<MatchMakingKeyValuePair_t> filters;

    };

    void ServerListResponse::getServerList(ServerListType slt) {
        p_impl->getServerList(slt);
    }
    void ServerListResponse::getServerList(ServerListType slt, const Filter &f) {
        p_impl->getServerList(slt, f);
    }
    void ServerListResponse::stopQuery() {
        p_impl->stopQuery();
    }
    ServerListResponse::ServerListResponse(ISteamMatchmakingServers *servers, ISteamMatchmakingServerListResponseCallback& responsecallback) {
        p_impl= new ServerListResponse::ServerListResponse_impl(servers, responsecallback);
    }

    ServerListResponse::~ServerListResponse() {
        delete p_impl;
    }



    class ModsQuery::ModsQueryImpl {
    public:

        ModsQueryImpl(const std::string &modspath) : modspath(modspath) {
        }


        void getLocalMods() {
            if(modsReceivedCallback &&
                    fs::exists(modspath) &&
                    fs::is_directory(modspath)) {
                fs::path tmp;
                for(auto iter=fs::directory_iterator(modspath) ; iter!= fs::directory_iterator(); iter++) {
                    tmp = *iter;
                    if(fs::is_directory(tmp))
                        checkModDir(tmp);
                }
            }
        }


        Mod readCPPFile(const fs::path &file) {
            Mod retval;
            std::ifstream infile(file.string().c_str());
            // tokens we want
            // name
            // hash ?

            if(infile) {
                std::string buffer;
                StringVector elem;
                while(std::getline(infile, buffer)) {
                    elem.clear();
                    split(buffer, '=', elem);
                    if(!elem.empty()) {
                        std::string key=elem[0];
                        boost::algorithm::trim(key);
                        if(boost::iequals(key, "name")) {
                            std::string val = elem[1];
                            boost::algorithm::trim(val);
                            retval.name = val;
                        }
                    }
                }
            }
            return retval;
        }


        void checkModDir(const fs::path &dir) {
            bool info_found=false;
            Mod mod;
            for(auto iter=fs::directory_iterator(dir) ; iter!= fs::directory_iterator(); iter++) {
                fs::path tmp(*iter);

                if(boost::iequals(tmp.filename().string(), "meta.cpp")) {
                    mod=readCPPFile(*iter);
                    if(!mod.name.empty())
                        info_found=true;
                }

                if(!info_found) {
                    if(boost::iequals(tmp.filename().string(), "mod.cpp")) {
                        mod=readCPPFile(*iter);
                        if(!mod.name.empty())
                            info_found=true;
                    }
                }

                if(!info_found) {
                    mod.name = dir.filename().string();
                }
            }
            mod.location = dir.string();
            mod.isworkshopitem = false;
            mods.push_back(mod);
        }


        void getMods(ModsReceivedCallback &mrc) {
            modsReceivedCallback = mrc;
            if(!mods.empty()) {
                if(mrc)
                    mrc(mods);
            } else {
                getLocalMods();
                //Mods retval;
                steam_ugc= SteamUGC();
                uint32 subcount = steam_ugc->GetNumSubscribedItems();
                uint32 i=steam_ugc->GetSubscribedItems(ugc, 1024);
                char directory[1024];
                qhandle = steam_ugc->CreateQueryUGCDetailsRequest(ugc, i);
                SteamAPICall_t cb=steam_ugc->SendQueryUGCRequest(qhandle);
                m_callResultSendQueryUGCRequest.Set(cb, this, &ModsQueryImpl::onModResult);
            }

        }
        void onModResult(SteamUGCQueryCompleted_t* cb, bool iets) {
            uint32 count = cb->m_unNumResultsReturned;

            for(uint32 i=0; i< count; i++) {
                SteamUGCDetails_t details;
                steam_ugc->GetQueryUGCResult( qhandle, i, &details);
                uint64 sizeOnDisk=0;
                char folder[1024];
                uint32 ts;
                steam_ugc->GetItemInstallInfo(details.m_nPublishedFileId, &sizeOnDisk, folder, 1024, &ts);
                if(details.m_nFileSize && details.m_rgchTitle[0] && folder[0] && !details.m_pchFileName[0])
                    mods.push_back(Mod(details.m_rgchTitle, folder));
            }
            if(modsReceivedCallback)
                modsReceivedCallback(mods);
        }

        void setChecked(size_t idx, bool checked) {
            Mod m=mods[idx];
            m.checked=!m.checked;
            mods[idx]=m;
            std::cout << m.name << " : " << m.checked << "\n";
        }

        std::string getCommand() {
            std::stringstream ss;
            std::string retval;
            bool first = true;
            for(auto it(mods.begin()); it!=mods.end(); it++ ) {
                if(it->checked) {
                    if(!first)
                        ss << "\\;";
                    else
                        first = false;
                    ss << fs::path("a3browser")/fs::basename(it->location);
                }
            }
            if(!first) {
                retval = "-mod=";
                retval += ss.str();
            }
            return retval;
        }


        ModsReceivedCallback modsReceivedCallback;
        PublishedFileId_t ugc[1024];
        CCallResult<ModsQueryImpl, SteamUGCQueryCompleted_t> m_callResultSendQueryUGCRequest;
        UGCQueryHandle_t qhandle;
        bool first=true;
        Mods mods;
        ISteamUGC*  steam_ugc=nullptr;
        std::string modspath;
    };

    SteamConnection::SteamConnection() {

    }

    SteamConnection::~SteamConnection() {
        SteamAPI_Shutdown();
    }
    void SteamConnection::init(std::function<void(bool)> initDoneAndSteamRunning_cb) {
        bool init_success = SteamAPI_Init();
        bool running = SteamAPI_IsSteamRunning();
        if(init_success && running) {
            if(initDoneAndSteamRunning_cb)
                initDoneAndSteamRunning_cb(true);
            apps = SteamApps();
        } else {
            if(initDoneAndSteamRunning_cb)
                initDoneAndSteamRunning_cb(false);
        }
    }

    ModsQuery_ptr SteamConnection::getInstalledMods(const std::string &modspath) {
        ModsQuery_ptr retval(new ModsQuery(modspath));
        return retval;
    }

    ServerListResponse_ptr SteamConnection::getServerListResponeObject(ISteamMatchmakingServerListResponseCallback &cbs) {
        if(!servers)
            servers = SteamMatchmakingServers();
        return ServerListResponse_ptr(new ServerListResponse(servers, cbs));
    }

    void SteamConnection::tickOnce() {
        SteamAPI_RunCallbacks();
    }


    void ModsQuery::setChecked(size_t idx, bool checked) {
        p_impl->setChecked(idx, checked);
    }
    std::string ModsQuery::getCommand() {
        return p_impl->getCommand();
    }

    void ModsQuery::getMods(ModsReceivedCallback &mrc) {
        p_impl->getMods(mrc);
    }

    const Mods& ModsQuery::getFoundMods() {
        return p_impl->mods;
    }

    ModsQuery::ModsQuery(const std::string &modspath) {
        p_impl = new ModsQueryImpl(modspath);
    }
    ModsQuery::~ModsQuery() {
        delete p_impl;
    }





    Settings::Settings() {
    }

#define QUOTE(A) #A
#define CAT(A,B)  QUOTE(A) "." QUOTE(B)


    void Settings::loadSettings() {
        try {

            if(!fs::exists(getConfigLocation())) {
                fs::create_directories(getConfigLocation());
            }
            if((fs::exists(getConfigLocation()/CONFIGFILE)) &&
                    (fs::is_regular_file(getConfigLocation()/CONFIGFILE))
              ) {

                pt::ptree tree;
                std::string configfile = (getConfigLocation()/CONFIGFILE).string();
                std::cerr << configfile << "\n";
                pt::read_xml(configfile, tree);

#define READSETTING(A) A = tree.get(CAT(Settings, A), A)
#define READPARAM(A) parameters.A = tree.get(CAT(Settings.param, A), parameters.A)

                READPARAM(enable_hyperthreading);
                READPARAM(no_splash);
                READPARAM(skip_intro);
                READPARAM(windowed);
                READPARAM(cores);
                READPARAM(threads);
                READPARAM(vblank);
                READPARAM(additional_parameters);

                READSETTING(mod_path);
                READSETTING(game_path);

                for(const pt::ptree::value_type &val : tree.get_child("Settings.EnabledMods") ) {
                    enabled_mods.insert(val.second.data());
                }
            }

        } catch(...) {

        }


    }

    void Settings::saveSettings() {
        try {

            if(!fs::exists(getConfigLocation()))
                fs::create_directories(getConfigLocation());

            pt::ptree tree;


#define WRITESETTING(A) tree.put(CAT(Settings,A), A);
#define WRITEMOD(A) tree.add(CAT(Settings.EnabledMods.mod, A), A);
#define WRITEPARAM(A) tree.put(CAT(Settings.param,A), parameters.A);

            WRITEPARAM(enable_hyperthreading);
            WRITEPARAM(no_splash);
            WRITEPARAM(skip_intro);
            WRITEPARAM(windowed);
            WRITEPARAM(cores);
            WRITEPARAM(threads);
            WRITEPARAM(vblank);
            WRITEPARAM(additional_parameters);

            WRITESETTING(mod_path);
            WRITESETTING(game_path);

            for(const std::string&name : enabled_mods) {
                WRITEMOD(name);
            }

            pt::write_xml((getConfigLocation()/CONFIGFILE).string(), tree);


        } catch(...) {

        }

    }

    std::string Settings::generateParametersString() {
        std::ostringstream ss;


        if(parameters.no_splash)
            ss << "-noSplash ";
        if(parameters.skip_intro)
            ss << "-skipIntro ";
        if(parameters.windowed)
            ss << "-window ";
        if(parameters.enable_hyperthreading)
            ss << "-enableHT ";
        if(parameters.cores > 0)
            ss << "-cpuCount=" << parameters.cores << " ";
        if(parameters.threads >0)
            ss << "-exThreads=" << parameters.threads << " ";
        if(parameters.vblank)
            ss << "-vblank_mode=1 ";
        else
            ss << "-vblank_mode=0 ";
        if(!parameters.additional_parameters.empty())
            ss << parameters.additional_parameters << " ";

        return ss.str();
    }
}
