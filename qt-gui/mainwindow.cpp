#include <io.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "serveritem.h"


enum CurrentTab{SERVERS, DOWNLOADS, SETTINGS, ABOUT} currentTab;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    InitUI();
}

// TODO Initializes some GUI elements like icons and
// TODO init from configuration file
void MainWindow::InitUI()
{
    // 1. page ----------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    //this->ui->
    this->ui->btnArma->setIcon(QIcon(":/ArmaLauncher/Icons/Arma3WhiteOnBlack.png"));
    //this->ui->btnArma->setIconSize(QSize(59, 59));

    this->ui->btnServers->setIcon(QIcon(":/ArmaLauncher/Icons/ServersIconColors2.png"));
    //this->ui->btnArma->setIconSize(QSize(59, 59));

    this->ui->btnDownloads->setIcon(QIcon(":/ArmaLauncher/Icons/DownloadsIcon.png"));
    //this->ui->btnArma->setIconSize(QSize(59, 59));

    this->ui->btnSettings->setIcon(QIcon(":/ArmaLauncher/Icons/SettingsIcon.png"));
    //this->ui->btnArma->setIconSize(QSize(59, 59));

    this->ui->btnAbout->setIcon(QIcon(":/ArmaLauncher/Icons/UpdateIcon.png"));
    //this->ui->btnArma->setIconSize(QSize(59, 59));


    this->ui->stackedWidget->setCurrentIndex(0);

    // Server table widget ------------------------------------------
    // Columns: favorite, locked, name, players, ping
    this->ui->twServers->setColumnCount(5);
    QStringList labels;
    labels << "Favorite" << "Locked" << "Server Name" << "Players" << "Ping";
    this->ui->twServers->setHorizontalHeaderLabels(labels);
    this->ui->twServers->setColumnWidth(0, 50);
    this->ui->twServers->setColumnWidth(1, 50);
    this->ui->twServers->setColumnWidth(2, 320);
    this->ui->twServers->setColumnWidth(3, 80);
//    this->ui->twServers->setColumnWidth(4, 60);


    // 2. page ----------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    // Server table widget ------------------------------------------
    // Columns: name, status, size, progress
    this->ui->twDownloads->setColumnCount(5);
    labels.clear();
    labels << "Name" << "Status" << "Size" << "Progress" << "Actions";
    this->ui->twDownloads->setHorizontalHeaderLabels(labels);
    this->ui->twDownloads->setColumnWidth(0, 240);
    this->ui->twDownloads->setColumnWidth(1, 120);
    this->ui->twDownloads->setColumnWidth(2, 120);
    this->ui->twDownloads->setColumnWidth(3, 160);
//    this->ui->twDownloads->setColumnWidth(4, 120);


    // 3. page ----------------------------------------------------------------------
    // ------------------------------------------------------------------------------




    // 4. page ----------------------------------------------------------------------
    // ------------------------------------------------------------------------------



    // Get servers ------------------------------------------------------------------
    //IO *io = new IO();
    // TODO
//    QVector<ServerItem> servers = io->Servers();
//    QSet<QString> favoriteServers = io->FavoriteServers();

//    for (int i = 0; i < servers.size(); i++)
//    {
////        servers[i]
//        if (servers[i].locked())
//        {
//            // TODO pridat ikonu zamku, treba najprv vyrobit
////            QTableWidgetItem *newItem = new QTableWidgetItem(servers[i].);
////            tableWidget->setItem(row, column, newItem);
//        }

//    }
}

void MainWindow::ChangeOtherIcons(QPushButton *btn)
{
    if (btn != this->ui->btnServers)
        this->ui->btnServers->setIcon(QIcon(":/ArmaLauncher/Icons/ServersIcon2.png"));

    if (btn != this->ui->btnDownloads)
        this->ui->btnDownloads->setIcon(QIcon(":/ArmaLauncher/Icons/DownloadsIcon.png"));

    if (btn != this->ui->btnSettings)
        this->ui->btnSettings->setIcon(QIcon(":/ArmaLauncher/Icons/SettingsIcon.png"));

    if (btn != this->ui->btnAbout)
        this->ui->btnAbout->setIcon(QIcon(":/ArmaLauncher/Icons/UpdateIcon.png"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

/// Buttons on the left ---------------------------------------------------------
void MainWindow::on_btnServers_clicked()
{
    if (ui->stackedWidget->currentIndex() != 0)
    {
        this->ui->stackedWidget->setCurrentIndex(0);

        this->ui->btnServers->setIcon(QIcon(":/ArmaLauncher/Icons/ServersIconColors2.png"));
        ChangeOtherIcons(this->ui->btnServers);
    }

}

void MainWindow::on_btnDownloads_clicked()
{
    if (ui->stackedWidget->currentIndex() != 1)
    {
        this->ui->stackedWidget->setCurrentIndex(1);

        this->ui->btnDownloads->setIcon(QIcon(":/ArmaLauncher/Icons/DownloadsIconColors.png"));
        ChangeOtherIcons(this->ui->btnDownloads);
    }
}

void MainWindow::on_btnSettings_clicked()
{
    if (ui->stackedWidget->currentIndex() != 2)
    {
        this->ui->stackedWidget->setCurrentIndex(2);

        this->ui->btnSettings->setIcon(QIcon(":/ArmaLauncher/Icons/SettingsIconColors.png"));
        ChangeOtherIcons(this->ui->btnSettings);
    }
}


void MainWindow::on_btnAbout_clicked()
{
    if (ui->stackedWidget->currentIndex() != 3)
    {
        this->ui->stackedWidget->setCurrentIndex(3);

        this->ui->btnAbout->setIcon(QIcon(":/ArmaLauncher/Icons/UpdateIconColors.png"));
        ChangeOtherIcons(this->ui->btnAbout);
    }
}


/// Search ------------------------------------------------------------------------
void MainWindow::on_btnSearch_clicked()
{

}


void MainWindow::on_btnClear_clicked()
{

}

void MainWindow::on_btnJoin_clicked()
{

}

QString MainWindow::GetLaunchParameter(QString ip, QString port, QStringList mods, QString password)
{
    // TODO ukldanie tychto parametrov
    QString resultCommand;

    resultCommand += "steam://107410/ ";

    // connection options
    resultCommand += "-connect=" + ip + " -port=" + port + " ";

    if (!password.isEmpty())
        resultCommand += "-password=" + password;

    // client options
    if(ui->cbNoSplash->isChecked())
        resultCommand += "-nosplash ";

    if(ui->cbSkipIntro->isChecked())
        resultCommand += "-skipIntro ";

    if(ui->cbWindowed->isChecked())
        resultCommand += "-window ";

    if(ui->cbHyperthreading->isChecked())
        resultCommand += "-enableHT ";

    if(ui->sbCores->value() != 0)
        resultCommand += "-cpuCount==" + QString(ui->sbCores->value()) + " ";

    if(ui->sbThreads->value() != 0)
        resultCommand += "-exThreads=" + QString(ui->sbThreads->value()) + " ";

    if(ui->rbOptirun->isChecked())
        resultCommand = "optirun " + resultCommand;

    if(ui->rbPrimus->isChecked())
        resultCommand = "primusrun " + resultCommand;

    if(ui->cbUnlockFPS->isChecked())
        resultCommand = "vblank_mode=0 " + resultCommand;

    // if the game will be launched with mods enabled
    if (mods.size() > 0)
    {
        resultCommand += "-mod=";
    }

    for (int i = 0; i < mods.size(); i++)
    {
        // TODO
        resultCommand += mods.at(i) + "\;";
    }

    return resultCommand;
}
