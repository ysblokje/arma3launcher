#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QDebug>
#include <QProcess>
#include <QtXml>

#include "io.h"

IO::IO(QObject *parent) :
    QObject(parent)
{

}

void IO::LoadConfig(QMainWindow *ui)
{
    settings_ = new QSettings("Moose Soft", "Clipper");

    settings_->beginGroup("MainWindow");
    ui->resize(settings_->value("size", QSize(400, 400)).toSize());
    ui->move(settings_->value("pos", QPoint(200, 200)).toPoint());
    settings_->endGroup();

    // TODO load favorite servers and also recent
}

void IO::SaveConfig(QMainWindow *ui)
{
    // TODO save config XML stuff

    settings_ = new QSettings("Moose Soft", "Clipper");

    settings_->beginGroup("MainWindow");
    settings_->setValue("size", ui->size());
    settings_->setValue("pos", ui->pos());
    settings_->endGroup();


    // TODO save favorite servers and also recent
}


void IO::DownloadModLinks()
{
//    Download();
}

void IO::LoadModLinks()
{

}

QVector<ServerItem> IO::Servers()
{
    QString link;

    // TODO
    int counter = 1;
    while (1)
    {
        Download("http://arma3.swec.se/server/list.xml?page=" + QString::number(counter) + "&sort=version", "list" + QString::number(counter) + ".xml");
        counter++;

        // Create a document to write XML
        QDomDocument document;

        // Open a file for reading
        QFile file("list" + QString::number(counter) + ".xml");
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "Failed to open the file for reading.";
        }
        else
        {
            // loading
            if(!document.setContent(&file))
            {
                qDebug() << "Failed to load the file for reading.";
            }
            file.close();
        }

        // Getting root element
        QDomElement root = document.firstChildElement();

        // retrievelements(QDomElement root, QString tag, QString att)
//        retrieveElements(root, "Dorm", "Name");

        QString tag = "Dorm";
        QString att = "Name";

        qDebug() << "Reading finished";



        // ------------------
        QDomNodeList nodes = root.elementsByTagName(tag);

        qDebug() << "# nodes = " << nodes.count();
        for(int i = 0; i < nodes.count(); i++)
        {
            QDomNode elm = nodes.at(i);
            if(elm.isElement())
            {
                QDomElement e = elm.toElement();
                qDebug() << e.attribute(att);
            }
        }
    }

}

QSet<QString> IO::FavoriteServers()
{

}

QSet<QString> IO::RecentServers()
{

}

void IO::AddToFavoriteServers(ServerItem item)
{

}

void IO::AddToRecentServers(ServerItem item)
{

}


void IO::CheckForUpdates()
{
    // TODO
}

void IO::Update()
{
    // TODO
}

void IO::Download(QString link, QString filename)
{
    if (link.contains("mega.nz", Qt::CaseInsensitive))
    {
        DownloadFromMega(link, filename);
    }
    else
        DownloadFromWeb(link, filename);
}

void IO::DownloadFromMega(QString link, QString filename)
{
    // TODO spustenie procesu ./megadl ...
    // po stiahnuti suboru ho treba premiestnit do spravneho adresara a taktiez ak
    // ho chcem znova stiahnut treba vymazat staru verziu

    // TODO rename

    QProcess *process = new QProcess(this);
    QString command = "./megadl " + link;
    process->start(command);

    connect(reply_, SIGNAL(downloadProgress(qint64,qint64)),
        new IO, SLOT(slUpdateDownloadStatus(qint64,qint64)));
}

/// Download a file
void IO::DownloadFromWeb(QString link, QString filename)
{
    // TODO finish the filedownloading thing
    QNetworkAccessManager *manager = new QNetworkAccessManager();

    networkManager_ = new QNetworkAccessManager();

    QUrl url(link);
    QFileInfo fileInfo(url.path());

    // filename and file part
    if (filename.isEmpty())
        QString filename = fileInfo.fileName();

    if (filename.isEmpty())
    {
        int ret = QMessageBox::warning(NULL, tr("My Application"),
                                         tr("The document has been modified.\n"
                                            "Do you want to save your changes?"),
                                         QMessageBox::Ok);
    }
    if (QFile::exists(filename))
    {
        QFile::remove(filename);
    }

    QFile *file = new QFile(filename);
    if (!file->open(QIODevice::WriteOnly))
    {
        QMessageBox::information(NULL, tr("HTTP"),
                                 tr("Unable to save the file %1: %2.")
                                 .arg(filename).arg(file->errorString()));
        delete file;
        file = 0;
        return;
    }
    // get() method posts a request
    // to obtain the contents of the target request
    // and returns a new QNetworkReply object
    // opened for reading which emits
    // the readyRead() signal whenever new data arrives.
    url_.setHost(link);
    reply_ = networkManager_->get(QNetworkRequest(url));
    // Whenever more data is received from the network,
    // this readyRead() signal is emitted
    connect(reply_, SIGNAL(readyRead()),
        new IO, SLOT(slHttpReadyRead()));

    // Also, downloadProgress() signal is emitted when data is received
    connect(reply_, SIGNAL(downloadProgress(qint64,qint64)),
        new IO, SLOT(slUpdateDownloadStatus(qint64,qint64)));

    // This signal is emitted when the reply_ has finished processing.
    // After this signal is emitted,
    // there will be no more updates to the reply_'s data or metadata.
    connect(reply_, SIGNAL(finished()),
        new IO, SLOT(slFinishDownloadStatus()));
}

/// File integrity
int IO::CheckFileIntegrity(QString filename, QString hashFilename)
{
    //  checks if file exists
    if (QFile::exists(filename))
    {
        QCryptographicHash crypto(QCryptographicHash::Sha512);
        QFile file(filename);
        file.open(QFile::ReadOnly);

        while(!file.atEnd())
        {
            crypto.addData(file.read(8192));
        }
        QString hash = crypto.result();
        file.close();

        // load the hash with which it needs to be compared
        file.open(QFile::ReadOnly);
        QString hashTarget = file.readAll();
        file.close();

        if (hash.compare(hashTarget) == 0)
        {
            qDebug() << "[OK] Checksum of file " << filename << " is ok";
            return 0;
        }
        else
        {
            qDebug() << "[WARNING] Checksum of file " << filename << " is different than it should be, file needs to be redownloaded !";
            return 1;
        }
    }
    else
    {
        qDebug() << "[WARNING] File " << filename << "does not exist !";
        return 1;
    }
}


/// SLOTS
void IO::slUpdateDownloadStatus(qint64 bytesRead, qint64 totalBytes)
{
    if (httpRequestAborted_)
        return;

    emit sigUpdateDownload(bytesRead, totalBytes);
//    progressDialog->setMaximum(totalBytes);
//    progressDialog->setValue(bytesRead);
}

void IO::slFinishDownloadStatus()
{
    // TODO hide progressbar
    // when canceled
    if (httpRequestAborted_)
    {
        if (file_)
        {
            file_->close();
            file_->remove();
            delete file_;
            file_ = 0;
        }
        reply_->deleteLater();
        return;
    }

    // download finished normally
    file_->flush();
    file_->close();

    // get redirection url
    QVariant redirectionTarget = reply_->attribute(QNetworkRequest::RedirectionTargetAttribute);

    if (reply_->error())
    {
        file_->remove();
        QMessageBox::information(NULL, tr("HTTP"),
                                 tr("Download failed: %1.").arg(reply_->errorString()));
    }
    else if (!redirectionTarget.isNull())
    {
        QUrl newUrl = url_.resolved(redirectionTarget.toUrl());
        if (QMessageBox::question(NULL, tr("HTTP"),
                                  tr("Redirect to %1 ?").arg(newUrl.toString()),
                                  QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
        {
            url_ = newUrl;
            reply_->deleteLater();
            file_->open(QIODevice::WriteOnly);
            file_->resize(0);
            Download(url_.toString());
            return;
        }
    }

    reply_->deleteLater();
    reply_ = 0;
    delete file_;
    file_ = 0;
    networkManager_ = 0;
}

void IO::slCancelDownload()
{
    httpRequestAborted_ = true;
    reply_->abort();

    sigAborted();
}

void IO::slHttpReadyRead()
{
    // this slot gets called every time the QNetworkReply has new data.
    // We read all of its new data and write it into the file.
    // That way we use less RAM than when reading it at the finished()
    // signal of the QNetworkReply
    if (file_)
        file_->write(reply_->readAll());
}


void IO::slUpdateCheckStatus()
{

}

void IO::slFinishCheckStatus()
{

}

void IO::slCancelCheck()
{

}
