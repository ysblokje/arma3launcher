#ifndef IO_H
#define IO_H

#include <QDialog>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QObject>
#include <QtXml/QDomEntity>
#include <QSettings>
#include <QMainWindow>
#include <QCryptographicHash>
#include <QSet>

#include "serveritem.h"


class IO : public QObject
{
    Q_OBJECT
public:
    explicit IO(QObject *parent = 0);

    // Loads the program configuration.
    void LoadConfig(QMainWindow *ui);
    // Saves the program configuration.
    void SaveConfig(QMainWindow *ui);

    // Downloads XML containing links for mods.
    void DownloadModLinks();
    // Loads the XML containing download links for mods.
    void LoadModLinks();

    QVector<ServerItem> Servers();
    // Returns the list of favorite servers.
    QSet<QString> FavoriteServers();
    // Returns the list of recent servers.
    QSet<QString> RecentServers();

    void AddToFavoriteServers(ServerItem item);
    void AddToRecentServers(ServerItem item);

    // Checks for updates.
    void CheckForUpdates();
    // Downloads new version.
    void Update();

    // Downloads file from a link.
    void Download(QString link, QString filename = "");

    // Download file from a web link.
    void DownloadFromWeb(QString link, QString filename = "");

    // Download file from a mega link.
    void DownloadFromMega(QString link, QString filename = "");

    // Checks mods integrity, using hash function.
    int CheckFileIntegrity(QString filename, QString hashFilename);



signals:
    void sigUpdateDownload(qint64 bytesRead, qint64 totalBytes);
    // Confirmation on abort.
    void sigAborted();

public slots:


private:
    QNetworkAccessManager *networkManager_;
    QUrl url_;
    QNetworkReply *reply_;
    QFile *file_;
    bool httpRequestAborted_;
    qint64 fileSize_;

    QSettings *settings_;

    QSet<QString> favoriteServers_;
    QSet<QString> recentServers_;

private slots:
    // Slot for update download/check status.
    void slUpdateDownloadStatus(qint64 bytesRead, qint64 totalBytes);
    // Slot for finish download/check.
    void slFinishDownloadStatus();
    // Cancel running download.
    void slCancelDownload();

    // Slot for update download/check status.
    void slUpdateCheckStatus();
    // Slot for finish download/check.
    void slFinishCheckStatus();
    // Cancel running download.
    void slCancelCheck();
    //
    void slHttpReadyRead();
};

#endif // IO_H
