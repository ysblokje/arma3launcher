#include "serveritem.h"

ServerItem::ServerItem()
{

}

void ServerItem::SetAttributes(QString name, bool locked, QString ip, QString port, QString country, QString map, unsigned int players, QString version)
{
    name_ = name;
    locked_ = locked;
    ip_ = ip;
    port_ = port;
    country_ = country;
    map_ = map;
    players_ = players;
    version_ = version;
}

QString ServerItem::name() const
{
    return name_;
}

void ServerItem::setName(const QString &name)
{
    name_ = name;
}

bool ServerItem::locked() const
{
    return locked_;
}

void ServerItem::setLocked(bool locked)
{
    locked_ = locked;
}

QString ServerItem::ip() const
{
    return ip_;
}

void ServerItem::setIp(const QString &ip)
{
    ip_ = ip;
}

QString ServerItem::port() const
{
    return port_;
}

void ServerItem::setPort(const QString &port)
{
    port_ = port;
}

QString ServerItem::country() const
{
    return country_;
}

void ServerItem::setCountry(const QString &country)
{
    country_ = country;
}

QString ServerItem::map() const
{
    return map_;
}

void ServerItem::setMap(const QString &map)
{
    map_ = map;
}

unsigned int ServerItem::players() const
{
    return players_;
}

void ServerItem::setPlayers(unsigned int players)
{
    players_ = players;
}

QString ServerItem::version() const
{
    return version_;
}

void ServerItem::setVersion(const QString &version)
{
    version_ = version;
}
