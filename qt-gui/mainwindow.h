#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <io.h>
#include "servers.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnServers_clicked();

    void on_btnDownloads_clicked();

    void on_btnSettings_clicked();

    void on_btnAbout_clicked();

    void on_btnSearch_clicked();

    void on_btnClear_clicked();

    void on_btnJoin_clicked();

private:
    Ui::MainWindow *ui;

    // returns the string that will launch the game.
    QString GetLaunchParameter(QString ip, QString port, QStringList mods, QString password = "");

    void InitUI();
    // Changes icons for other buttons than btn to the inactive icon
    void ChangeOtherIcons(QPushButton *btn);

};

#endif // MAINWINDOW_H
