#-------------------------------------------------
#
# Project created by QtCreator 2016-03-21T18:18:50
#
#-------------------------------------------------

QT       += core gui network xml
DEFINES  += QT_NO_SSL


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Arma3LinuxLauncher
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialogpassword.cpp \
    moddownloaddialog.cpp \
    servers.cpp \
    io.cpp \
    serveritem.cpp

HEADERS  += mainwindow.h \
    dialogpassword.h \
    moddownloaddialog.h \
    servers.h \
    io.h \
    serveritem.h

FORMS    += mainwindow.ui \
    dialogpassword.ui \
    moddownloaddialog.ui

OTHER_FILES += \
    TODO.txt

RESOURCES += \
    Icons.qrc

DISTFILES += \
    Readme


