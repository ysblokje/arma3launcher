#ifndef SERVERS_H
#define SERVERS_H

#include <QString>

#include <io.h>

// This class serves for storing information about a server.
class Server
{

};


class Servers
{
public:
    Servers();

    void RefreshServers();
    QList<Server> GetServers(QString version, QList<QString> mods, int ping);


    void GetInternetServers();
    void GetLANServers();

private:
    IO *io_;
};


#endif // SERVERS_H
