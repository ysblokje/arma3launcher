#ifndef SERVERITEM_H
#define SERVERITEM_H

#include <QString>
#include <QStringList>

/* Serves for storing data about particular servers.
*/
class ServerItem
{
public:
    ServerItem();

    void SetAttributes(QString name, bool locked, QString ip, QString port, QString country, QString map, unsigned int players, QString version);

    QString name() const;
    void setName(const QString &name);

    bool locked() const;
    void setLocked(bool locked);

    QString ip() const;
    void setIp(const QString &ip);

    QString port() const;
    void setPort(const QString &port);

    QString country() const;
    void setCountry(const QString &country);

    QString map() const;
    void setMap(const QString &map);

    unsigned int players() const;
    void setPlayers(unsigned int players);

    QString version() const;
    void setVersion(const QString &version);

private:
    QString name_;
    bool locked_;
    QString ip_;
    QString port_;
    QString country_;
    QString map_;
    unsigned int players_;
    QString version_;
    QStringList mods_;      // mods the player enabled for this particular server
};

#endif // SERVERITEM_H
