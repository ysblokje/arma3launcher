///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 10 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "A3Launcher.h"

#include "DownloadsIconColors.png.h"
#include "ServersIconColors2b.png.h"
#include "SettingsIcon.png.h"

///////////////////////////////////////////////////////////////////////////

MainFrame::MainFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxVERTICAL );
	
	m_notebook1 = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_FLAT|wxNB_LEFT|wxNB_TOP );
	wxSize m_notebook1ImageSize = wxSize( 50,50 );
	int m_notebook1Index = 0;
	wxImageList* m_notebook1Images = new wxImageList( m_notebook1ImageSize.GetWidth(), m_notebook1ImageSize.GetHeight() );
	m_notebook1->AssignImageList( m_notebook1Images );
	wxBitmap m_notebook1Bitmap;
	wxImage m_notebook1Image;
	m_serverpanel = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, wxT("Servers") );
	m_serverpanel->SetToolTip( wxT("Servers") );
	
	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText1 = new wxStaticText( m_serverpanel, wxID_ANY, wxT("Search:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	bSizer20->Add( m_staticText1, 0, wxALL, 5 );
	
	m_textCtrl4 = new wxTextCtrl( m_serverpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_textCtrl4, 0, wxALL|wxEXPAND, 5 );
	
	m_checkBox1 = new wxCheckBox( m_serverpanel, wxID_ANY, wxT("Show full"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_checkBox1, 0, wxALL, 5 );
	
	m_checkBox2 = new wxCheckBox( m_serverpanel, wxID_ANY, wxT("Show empty"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_checkBox2, 0, wxALL, 5 );
	
	m_checkBox3 = new wxCheckBox( m_serverpanel, wxID_ANY, wxT("Show passworded"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_checkBox3, 0, wxALL, 5 );
	
	m_showbattleeye = new wxCheckBox( m_serverpanel, wxID_ANY, wxT("Show BattleEye"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_showbattleeye, 0, wxALL, 5 );
	
	wxString m_serverlisttypeChoices[] = { wxT("Internet"), wxT("Favorite"), wxT("Friends"), wxT("History"), wxT("LAN") };
	int m_serverlisttypeNChoices = sizeof( m_serverlisttypeChoices ) / sizeof( wxString );
	m_serverlisttype = new wxRadioBox( m_serverpanel, wxID_ANY, wxT("Servers"), wxDefaultPosition, wxDefaultSize, m_serverlisttypeNChoices, m_serverlisttypeChoices, 1, wxRA_SPECIFY_COLS|wxNO_BORDER );
	m_serverlisttype->SetSelection( 0 );
	bSizer20->Add( m_serverlisttype, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText81 = new wxStaticText( m_serverpanel, wxID_ANY, wxT("Version"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText81->Wrap( -1 );
	bSizer20->Add( m_staticText81, 0, wxALL, 5 );
	
	wxString m_versionfilterChoices[] = { wxT("any"), wxT("154"), wxT("160"), wxT("162") };
	int m_versionfilterNChoices = sizeof( m_versionfilterChoices ) / sizeof( wxString );
	m_versionfilter = new wxChoice( m_serverpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_versionfilterNChoices, m_versionfilterChoices, 0 );
	m_versionfilter->SetSelection( 1 );
	bSizer20->Add( m_versionfilter, 0, wxALL|wxEXPAND, 5 );
	
	
	bSizer20->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_button5 = new wxButton( m_serverpanel, wxID_ANY, wxT("Search"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button5->SetDefault(); 
	bSizer20->Add( m_button5, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_button6 = new wxButton( m_serverpanel, wxID_ANY, wxT("Refresh"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_button6, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_stopserverlisting = new wxButton( m_serverpanel, wxID_ANY, wxT("Stop"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_stopserverlisting, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_button7 = new wxButton( m_serverpanel, wxID_ANY, wxT("Clear filters"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer20->Add( m_button7, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_staticline1 = new wxStaticLine( m_serverpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bSizer20->Add( m_staticline1, 0, wxEXPAND | wxALL, 5 );
	
	m_launch = new wxButton( m_serverpanel, wxID_ANY, wxT("Launch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_launch->SetToolTip( wxT("Launch game") );
	
	bSizer20->Add( m_launch, 0, wxALIGN_CENTER|wxALL, 5 );
	
	m_button8 = new wxButton( m_serverpanel, wxID_ANY, wxT("Join"), wxDefaultPosition, wxDefaultSize, 0 );
	m_button8->SetToolTip( wxT("Join selected server") );
	
	bSizer20->Add( m_button8, 0, wxALIGN_CENTER|wxALL, 5 );
	
	
	bSizer3->Add( bSizer20, 0, wxALIGN_BOTTOM|wxBOTTOM|wxEXPAND|wxFIXED_MINSIZE, 5 );
	
	m_splitter2 = new wxSplitterWindow( m_serverpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter2->Connect( wxEVT_IDLE, wxIdleEventHandler( MainFrame::m_splitter2OnIdle ), NULL, this );
	
	m_panel8 = new wxPanel( m_splitter2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* serverlistsizer;
	serverlistsizer = new wxBoxSizer( wxVERTICAL );
	
	m_serverlist = new wxListCtrl( m_panel8, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_SINGLE_SEL|wxLC_SORT_ASCENDING|wxLC_SORT_DESCENDING );
	serverlistsizer->Add( m_serverlist, 4, wxEXPAND, 5 );
	
	
	m_panel8->SetSizer( serverlistsizer );
	m_panel8->Layout();
	serverlistsizer->Fit( m_panel8 );
	m_panel9 = new wxPanel( m_splitter2, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* serverrulessizer;
	serverrulessizer = new wxBoxSizer( wxVERTICAL );
	
	m_modlist = new wxListCtrl( m_panel9, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_SINGLE_SEL );
	serverrulessizer->Add( m_modlist, 1, wxALL|wxEXPAND, 5 );
	
	m_serverrules = new wxListCtrl( m_panel9, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT|wxLC_SINGLE_SEL|wxLC_SORT_ASCENDING|wxLC_SORT_DESCENDING );
	serverrulessizer->Add( m_serverrules, 2, wxEXPAND, 10 );
	
	
	m_panel9->SetSizer( serverrulessizer );
	m_panel9->Layout();
	serverrulessizer->Fit( m_panel9 );
	m_splitter2->SplitVertically( m_panel8, m_panel9, 0 );
	bSizer3->Add( m_splitter2, 1, wxEXPAND, 5 );
	
	
	m_serverpanel->SetSizer( bSizer3 );
	m_serverpanel->Layout();
	bSizer3->Fit( m_serverpanel );
	m_notebook1->AddPage( m_serverpanel, wxEmptyString, true );
	m_notebook1Bitmap = ServersIconColors2b_png_to_wx_bitmap();
	if ( m_notebook1Bitmap.Ok() )
	{
		m_notebook1Image = m_notebook1Bitmap.ConvertToImage();
		m_notebook1Images->Add( m_notebook1Image.Scale( m_notebook1ImageSize.GetWidth(), m_notebook1ImageSize.GetHeight() ) );
		m_notebook1->SetPageImage( m_notebook1Index, m_notebook1Index );
		m_notebook1Index++;
	}
	m_modspanel = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_modspanel->SetToolTip( wxT("Mods") );
	
	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxVERTICAL );
	
	wxArrayString m_checkedmodlistChoices;
	m_checkedmodlist = new wxCheckListBox( m_modspanel, wxID_MODS, wxDefaultPosition, wxDefaultSize, m_checkedmodlistChoices, wxLB_ALWAYS_SB|wxLB_MULTIPLE );
	bSizer21->Add( m_checkedmodlist, 1, wxALL|wxEXPAND, 5 );
	
	
	m_modspanel->SetSizer( bSizer21 );
	m_modspanel->Layout();
	bSizer21->Fit( m_modspanel );
	m_notebook1->AddPage( m_modspanel, wxEmptyString, false );
	m_notebook1Bitmap = DownloadsIconColors_png_to_wx_bitmap();
	if ( m_notebook1Bitmap.Ok() )
	{
		m_notebook1Image = m_notebook1Bitmap.ConvertToImage();
		m_notebook1Images->Add( m_notebook1Image.Scale( m_notebook1ImageSize.GetWidth(), m_notebook1ImageSize.GetHeight() ) );
		m_notebook1->SetPageImage( m_notebook1Index, m_notebook1Index );
		m_notebook1Index++;
	}
	m_settingspanel = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_settingspanel->SetToolTip( wxT("Settings") );
	
	wxBoxSizer* bSizer211;
	bSizer211 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2 = new wxStaticText( m_settingspanel, wxID_ANY, wxT("Path to Arma3 folder"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer211->Add( m_staticText2, 0, wxALL, 5 );
	
	m_dir_arma3 = new wxDirPickerCtrl( m_settingspanel, wxID_ANY, wxT("/home/minze"), wxT("Select a folder"), wxDefaultPosition, wxDefaultSize, wxDIRP_DEFAULT_STYLE );
	bSizer211->Add( m_dir_arma3, 0, wxALL|wxEXPAND, 5 );
	
	m_staticText3 = new wxStaticText( m_settingspanel, wxID_ANY, wxT("Path to Arma3 Mods"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	bSizer211->Add( m_staticText3, 0, wxALL, 5 );
	
	m_dir_mods = new wxDirPickerCtrl( m_settingspanel, wxID_ANY, wxEmptyString, wxT("Select a folder"), wxDefaultPosition, wxDefaultSize, wxDIRP_DEFAULT_STYLE );
	bSizer211->Add( m_dir_mods, 0, wxALL|wxEXPAND, 5 );
	
	
	bSizer211->Add( 0, 0, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer22;
	bSizer22 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText8 = new wxStaticText( m_settingspanel, wxID_ANY, wxT("Arma3 parameters"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	bSizer24->Add( m_staticText8, 0, wxALL, 5 );
	
	m_enable_hyperthreading = new wxCheckBox( m_settingspanel, wxID_ANY, wxT("Enable hyperthreading"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer24->Add( m_enable_hyperthreading, 0, wxALL, 5 );
	
	m_no_splash = new wxCheckBox( m_settingspanel, wxID_ANY, wxT("noSplash"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer24->Add( m_no_splash, 0, wxALL, 5 );
	
	m_skip_intro = new wxCheckBox( m_settingspanel, wxID_ANY, wxT("skipIntro"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer24->Add( m_skip_intro, 0, wxALL, 5 );
	
	m_windowed = new wxCheckBox( m_settingspanel, wxID_ANY, wxT("windowed"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer24->Add( m_windowed, 0, wxALL, 5 );
	
	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 0, 3, 0, 0 );
	
	m_staticText4 = new wxStaticText( m_settingspanel, wxID_ANY, wxT("Number of cores"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	gSizer1->Add( m_staticText4, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_cores = new wxSpinCtrl( m_settingspanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	gSizer1->Add( m_cores, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	
	gSizer1->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_staticText41 = new wxStaticText( m_settingspanel, wxID_ANY, wxT("Number of extra threads"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText41->Wrap( -1 );
	gSizer1->Add( m_staticText41, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_threads = new wxSpinCtrl( m_settingspanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 0 );
	gSizer1->Add( m_threads, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	
	bSizer24->Add( gSizer1, 0, wxEXPAND, 5 );
	
	
	bSizer22->Add( bSizer24, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxVERTICAL );
	
	wxString m_radioBox3Choices[] = { wxT("Use official mirror if possible"), wxT("Use Google Drive / Mega if possible (might be faster)") };
	int m_radioBox3NChoices = sizeof( m_radioBox3Choices ) / sizeof( wxString );
	m_radioBox3 = new wxRadioBox( m_settingspanel, wxID_ANY, wxT("Download servers"), wxDefaultPosition, wxDefaultSize, m_radioBox3NChoices, m_radioBox3Choices, 1, wxRA_SPECIFY_COLS|wxNO_BORDER );
	m_radioBox3->SetSelection( 1 );
	bSizer23->Add( m_radioBox3, 0, wxALL|wxEXPAND, 5 );
	
	wxString m_radioBox4Choices[] = { wxT("primusrun"), wxT("optirun"), wxT("none") };
	int m_radioBox4NChoices = sizeof( m_radioBox4Choices ) / sizeof( wxString );
	m_radioBox4 = new wxRadioBox( m_settingspanel, wxID_ANY, wxT("Nvidia bumblebee"), wxDefaultPosition, wxDefaultSize, m_radioBox4NChoices, m_radioBox4Choices, 1, wxRA_SPECIFY_COLS|wxNO_BORDER );
	m_radioBox4->SetSelection( 2 );
	bSizer23->Add( m_radioBox4, 0, wxALL|wxEXPAND, 5 );
	
	m_vblank = new wxCheckBox( m_settingspanel, wxID_ANY, wxT("Unlocked FPS"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer23->Add( m_vblank, 0, wxALL, 5 );
	
	
	bSizer22->Add( bSizer23, 1, wxEXPAND, 5 );
	
	
	bSizer211->Add( bSizer22, 4, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer29;
	bSizer29 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText7 = new wxStaticText( m_settingspanel, wxID_ANY, wxT("Additional parameters"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	bSizer29->Add( m_staticText7, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	m_additional_parameters = new wxTextCtrl( m_settingspanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer29->Add( m_additional_parameters, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5 );
	
	
	bSizer211->Add( bSizer29, 0, wxEXPAND, 5 );
	
	
	bSizer211->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_launch1 = new wxButton( m_settingspanel, wxID_ANY, wxT("Launch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_launch1->SetToolTip( wxT("Launch game") );
	
	bSizer211->Add( m_launch1, 0, wxALL, 5 );
	
	
	m_settingspanel->SetSizer( bSizer211 );
	m_settingspanel->Layout();
	bSizer211->Fit( m_settingspanel );
	m_notebook1->AddPage( m_settingspanel, wxEmptyString, false );
	m_notebook1Bitmap = SettingsIcon_png_to_wx_bitmap();
	if ( m_notebook1Bitmap.Ok() )
	{
		m_notebook1Image = m_notebook1Bitmap.ConvertToImage();
		m_notebook1Images->Add( m_notebook1Image.Scale( m_notebook1ImageSize.GetWidth(), m_notebook1ImageSize.GetHeight() ) );
		m_notebook1->SetPageImage( m_notebook1Index, m_notebook1Index );
		m_notebook1Index++;
	}
	
	bSizer7->Add( m_notebook1, 1, wxEXPAND | wxALL, 5 );
	
	
	this->SetSizer( bSizer7 );
	this->Layout();
	m_statusBar1 = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	
	this->Centre( wxBOTH );
	
	// Connect Events
	m_button5->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onServersClicked ), NULL, this );
	m_stopserverlisting->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onServersStopClicked ), NULL, this );
	m_launch->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onLaunchClicked ), NULL, this );
	m_button8->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onJoinClicked ), NULL, this );
	m_serverlist->Connect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( MainFrame::onColumnClicked ), NULL, this );
	m_serverlist->Connect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrame::onItemActivated ), NULL, this );
	m_serverlist->Connect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( MainFrame::onItemDeselected ), NULL, this );
	m_serverlist->Connect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( MainFrame::onItemSelected ), NULL, this );
	m_serverrules->Connect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( MainFrame::onColumnClicked ), NULL, this );
	m_serverrules->Connect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrame::onItemActivated ), NULL, this );
	m_serverrules->Connect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( MainFrame::onItemDeselected ), NULL, this );
	m_serverrules->Connect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( MainFrame::onItemSelected ), NULL, this );
	m_modspanel->Connect( wxEVT_SET_FOCUS, wxFocusEventHandler( MainFrame::onModFocus ), NULL, this );
	m_checkedmodlist->Connect( wxEVT_COMMAND_CHECKLISTBOX_TOGGLED, wxCommandEventHandler( MainFrame::onModToggled ), NULL, this );
	m_dir_arma3->Connect( wxEVT_COMMAND_DIRPICKER_CHANGED, wxFileDirPickerEventHandler( MainFrame::onArmaPathChanged ), NULL, this );
	m_dir_mods->Connect( wxEVT_COMMAND_DIRPICKER_CHANGED, wxFileDirPickerEventHandler( MainFrame::onArmaModChanged ), NULL, this );
	m_enable_hyperthreading->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onEnableHyperThreading ), NULL, this );
	m_no_splash->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onNoSplash ), NULL, this );
	m_skip_intro->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onSkipIntro ), NULL, this );
	m_windowed->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onWindowed ), NULL, this );
	m_cores->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( MainFrame::onCores ), NULL, this );
	m_threads->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( MainFrame::onThreads ), NULL, this );
	m_vblank->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onVBlank ), NULL, this );
	m_additional_parameters->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrame::onAdditionalParameters ), NULL, this );
	m_launch1->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onLaunchClicked ), NULL, this );
}

MainFrame::~MainFrame()
{
	// Disconnect Events
	m_button5->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onServersClicked ), NULL, this );
	m_stopserverlisting->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onServersStopClicked ), NULL, this );
	m_launch->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onLaunchClicked ), NULL, this );
	m_button8->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onJoinClicked ), NULL, this );
	m_serverlist->Disconnect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( MainFrame::onColumnClicked ), NULL, this );
	m_serverlist->Disconnect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrame::onItemActivated ), NULL, this );
	m_serverlist->Disconnect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( MainFrame::onItemDeselected ), NULL, this );
	m_serverlist->Disconnect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( MainFrame::onItemSelected ), NULL, this );
	m_serverrules->Disconnect( wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler( MainFrame::onColumnClicked ), NULL, this );
	m_serverrules->Disconnect( wxEVT_COMMAND_LIST_ITEM_ACTIVATED, wxListEventHandler( MainFrame::onItemActivated ), NULL, this );
	m_serverrules->Disconnect( wxEVT_COMMAND_LIST_ITEM_DESELECTED, wxListEventHandler( MainFrame::onItemDeselected ), NULL, this );
	m_serverrules->Disconnect( wxEVT_COMMAND_LIST_ITEM_SELECTED, wxListEventHandler( MainFrame::onItemSelected ), NULL, this );
	m_modspanel->Disconnect( wxEVT_SET_FOCUS, wxFocusEventHandler( MainFrame::onModFocus ), NULL, this );
	m_checkedmodlist->Disconnect( wxEVT_COMMAND_CHECKLISTBOX_TOGGLED, wxCommandEventHandler( MainFrame::onModToggled ), NULL, this );
	m_dir_arma3->Disconnect( wxEVT_COMMAND_DIRPICKER_CHANGED, wxFileDirPickerEventHandler( MainFrame::onArmaPathChanged ), NULL, this );
	m_dir_mods->Disconnect( wxEVT_COMMAND_DIRPICKER_CHANGED, wxFileDirPickerEventHandler( MainFrame::onArmaModChanged ), NULL, this );
	m_enable_hyperthreading->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onEnableHyperThreading ), NULL, this );
	m_no_splash->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onNoSplash ), NULL, this );
	m_skip_intro->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onSkipIntro ), NULL, this );
	m_windowed->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onWindowed ), NULL, this );
	m_cores->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( MainFrame::onCores ), NULL, this );
	m_threads->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( MainFrame::onThreads ), NULL, this );
	m_vblank->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( MainFrame::onVBlank ), NULL, this );
	m_additional_parameters->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( MainFrame::onAdditionalParameters ), NULL, this );
	m_launch1->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::onLaunchClicked ), NULL, this );
	
}
