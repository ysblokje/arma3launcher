///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 10 2016)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __A3LAUNCHER_H__
#define __A3LAUNCHER_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/radiobox.h>
#include <wx/choice.h>
#include <wx/button.h>
#include <wx/statline.h>
#include <wx/sizer.h>
#include <wx/listctrl.h>
#include <wx/panel.h>
#include <wx/splitter.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/checklst.h>
#include <wx/filepicker.h>
#include <wx/spinctrl.h>
#include <wx/notebook.h>
#include <wx/imaglist.h>
#include <wx/statusbr.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class MainFrame
///////////////////////////////////////////////////////////////////////////////
class MainFrame : public wxFrame 
{
	private:
	
	protected:
		enum
		{
			wxID_MODS = 1000
		};
		
		wxNotebook* m_notebook1;
		wxPanel* m_serverpanel;
		wxStaticText* m_staticText1;
		wxTextCtrl* m_textCtrl4;
		wxCheckBox* m_checkBox1;
		wxCheckBox* m_checkBox2;
		wxCheckBox* m_checkBox3;
		wxCheckBox* m_showbattleeye;
		wxRadioBox* m_serverlisttype;
		wxStaticText* m_staticText81;
		wxChoice* m_versionfilter;
		wxButton* m_button5;
		wxButton* m_button6;
		wxButton* m_stopserverlisting;
		wxButton* m_button7;
		wxStaticLine* m_staticline1;
		wxButton* m_launch;
		wxButton* m_button8;
		wxSplitterWindow* m_splitter2;
		wxPanel* m_panel8;
		wxPanel* m_panel9;
		wxListCtrl* m_modlist;
		wxPanel* m_modspanel;
		wxPanel* m_settingspanel;
		wxStaticText* m_staticText2;
		wxDirPickerCtrl* m_dir_arma3;
		wxStaticText* m_staticText3;
		wxDirPickerCtrl* m_dir_mods;
		wxStaticText* m_staticText8;
		wxCheckBox* m_enable_hyperthreading;
		wxCheckBox* m_no_splash;
		wxCheckBox* m_skip_intro;
		wxCheckBox* m_windowed;
		wxStaticText* m_staticText4;
		wxSpinCtrl* m_cores;
		wxStaticText* m_staticText41;
		wxSpinCtrl* m_threads;
		wxRadioBox* m_radioBox3;
		wxRadioBox* m_radioBox4;
		wxCheckBox* m_vblank;
		wxStaticText* m_staticText7;
		wxTextCtrl* m_additional_parameters;
		wxButton* m_launch1;
		wxStatusBar* m_statusBar1;
		
		// Virtual event handlers, overide them in your derived class
		virtual void onServersClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void onServersStopClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void onLaunchClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void onJoinClicked( wxCommandEvent& event ) { event.Skip(); }
		virtual void onColumnClicked( wxListEvent& event ) { event.Skip(); }
		virtual void onItemActivated( wxListEvent& event ) { event.Skip(); }
		virtual void onItemDeselected( wxListEvent& event ) { event.Skip(); }
		virtual void onItemSelected( wxListEvent& event ) { event.Skip(); }
		virtual void onModFocus( wxFocusEvent& event ) { event.Skip(); }
		virtual void onModToggled( wxCommandEvent& event ) { event.Skip(); }
		virtual void onArmaPathChanged( wxFileDirPickerEvent& event ) { event.Skip(); }
		virtual void onArmaModChanged( wxFileDirPickerEvent& event ) { event.Skip(); }
		virtual void onEnableHyperThreading( wxCommandEvent& event ) { event.Skip(); }
		virtual void onNoSplash( wxCommandEvent& event ) { event.Skip(); }
		virtual void onSkipIntro( wxCommandEvent& event ) { event.Skip(); }
		virtual void onWindowed( wxCommandEvent& event ) { event.Skip(); }
		virtual void onCores( wxSpinEvent& event ) { event.Skip(); }
		virtual void onThreads( wxSpinEvent& event ) { event.Skip(); }
		virtual void onVBlank( wxCommandEvent& event ) { event.Skip(); }
		virtual void onAdditionalParameters( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		wxListCtrl* m_serverlist;
		wxListCtrl* m_serverrules;
		wxCheckListBox* m_checkedmodlist;
		
		MainFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 873,660 ), long style = wxCLOSE_BOX|wxDEFAULT_FRAME_STYLE|wxMAXIMIZE|wxMINIMIZE|wxRESIZE_BORDER|wxTAB_TRAVERSAL );
		
		~MainFrame();
		
		void m_splitter2OnIdle( wxIdleEvent& )
		{
			m_splitter2->SetSashPosition( 0 );
			m_splitter2->Disconnect( wxEVT_IDLE, wxIdleEventHandler( MainFrame::m_splitter2OnIdle ), NULL, this );
		}
	
};

#endif //__A3LAUNCHER_H__
