#include <iostream>
#include <vector>
#include <sstream>
#include <functional>
#include <unistd.h>

#include "A3Launcher.h"
#include "SteamQueries.hpp"
#include <wx/wx.h>
#include <wx/app.h>
#include <boost/filesystem.hpp>


using namespace arma3;

class ArmaLauncher: public wxApp {
public:
    ArmaLauncher();
    ~ArmaLauncher();
    bool OnInit();
    void handlerFuncName(wxIdleEvent& event);
protected:

private:
    wxFrame* frame=nullptr;
};


class CallbackTicker : public wxTimer {
public:
    CallbackTicker(std::function<void()> cb) : cb(cb) {}

    void Notify() {
        if(cb)
            cb();
    }

    void Run() {
        wxTimer::Start(10);
    }

    std::function<void()> cb;
};


std::vector<GameServer_ptr> items;

int compare(wxIntPtr item1, wxIntPtr item2, wxIntPtr sortData) {
    GameServer_ptr item1_ptr=(items[item1]);
    GameServer_ptr item2_ptr=(items[item2]);
    if(sortData==0) {
        if(item1_ptr->getPing()<item2_ptr->getPing())
            return -1;
        else if(item1_ptr->getPing()>item2_ptr->getPing())
            return 1;
    } else if(sortData==1) {
        return strcmp(item1_ptr->getName().c_str(),item2_ptr->getName().c_str());
    } else if(sortData==2) {
        return strcmp(item1_ptr->getVersion().c_str(),item2_ptr->getVersion().c_str());
    }
    return 0;
}




class MyMainFrame : public MainFrame {
    bool first = true;
    arma3::Settings settings;
public :

    ~MyMainFrame() {
        settings.game_path = m_dir_arma3->GetPath().ToUTF8();
        settings.mod_path  = m_dir_mods->GetPath().ToUTF8();
        settings.saveSettings();
    }

    MyMainFrame() :  MainFrame(nullptr)  {
        m_serverlist->InsertColumn(0, "ping");
        m_serverlist->InsertColumn(1, "name");
        m_serverlist->InsertColumn(2, "version");
        m_serverlist->InsertColumn(3, "gametype");
        m_serverlist->InsertColumn(4, "BattleEye");

        m_serverrules->InsertColumn(0, "key");
        m_serverrules->InsertColumn(1, "value");

        m_modlist->InsertColumn(0, "Name");
        m_modlist->InsertColumn(1, "Hash");
        m_modlist->InsertColumn(2, "Present");

        settings.loadSettings();

        m_dir_mods->SetPath(settings.mod_path);
        m_dir_arma3->SetPath(settings.game_path);

        settings.parameters.enable_hyperthreading = m_enable_hyperthreading->IsChecked();

        m_no_splash->SetValue(settings.parameters.no_splash);
        m_skip_intro->SetValue(settings.parameters.skip_intro);
        m_windowed->SetValue(settings.parameters.windowed);
        m_cores->SetValue(settings.parameters.cores);
        m_threads->SetValue(settings.parameters.threads);
        m_additional_parameters->SetValue(settings.parameters.additional_parameters);
        m_vblank->SetValue(!settings.parameters.vblank);
    }

    void onSteamInitDone(bool success) {
        if(success) {
            if(first) {
                first = false;
                ticker = new CallbackTicker([this]() {
                    steam.tickOnce();
                });
                ticker->Run();
            }
        }
    }

    void startScan() {
        callbacks.ServerResponded_cb = [this](GameServer_ptr details) {
            GameServer_ptr tmpd(details);
            items.push_back(tmpd);
            wxListItem item;
            item.SetId(0);
            item.SetData(items.size()-1);
            item.SetText(wxString() <<(details->getPing()));
            long tmp=m_serverlist->InsertItem(item);

            // const char* naam = details.//name.c_str();
            item.SetId(tmp);
            item.SetColumn(1);
            item.SetText(wxString(details->getName().c_str()));

            m_serverlist->SetItem(item);

            item.SetColumn(2);
            item.SetText(wxString(tmpd->getVersion()));
            m_serverlist->SetItem(item);
            item.SetColumn(3);
            item.SetText(wxString(tmpd->getGameType()));
            m_serverlist->SetItem(item);
            item.SetColumn(4);
            item.SetText(wxString((tmpd->usesBattleEye()?"true":"false")));
            m_serverlist->SetItem(item);
            //item.SetColumn(5);
            //item.SetText(wxString(wxString(tmpd.m_szGameTags)));
            //m_serverlist->SetItem(item);

            wxApp::GetInstance()->Yield();
        };

        server_response = steam.getServerListResponeObject(callbacks);
        ServerListResponse::ServerListType t(static_cast<ServerListResponse::ServerListType>(m_serverlisttype->GetSelection()));
        ServerListResponse::Filter f;
        int selection = m_versionfilter->GetSelection();
        if(selection>0) {
            wxString version = m_versionfilter->GetString(selection);
            f.version = version.ToAscii();
        }
        f.showbattleye = m_showbattleeye->IsChecked();
        server_response->getServerList(t, f);

    }

    bool init_started=false;
    void onIdle(wxIdleEvent&evt) {
        if(!init_started) {
            init_started=true;
            steam.init([this](bool success) {
                scanMods();
                onSteamInitDone(success);
            });

        }
    }

    void onServersClicked(wxCommandEvent& event) {
        startScan();
    }
    void onServersStopClicked(wxCommandEvent& event) {
        if(server_response.get())
            server_response->stopQuery();
    }

    void onColumnClicked( wxListEvent& event ) {
        int column = event.GetColumn();
        std::cout << "pressed " << column << "\n";

        wxListCtrlCompare lcc=compare;
        m_serverlist->SortItems(lcc, wxIntPtr(column));
    }


    void onItemSelected( wxListEvent& event ) {
        GameServer_ptr gs=items[event.GetData()];
        m_serverrules->DeleteAllItems();
        m_modlist->DeleteAllItems();


        rules_callbacks.RefreshComplete_cb = [this](ServerDetails &qr) {
            Mods mods_tmp;
            if(!m_modsfound.empty())
                mods_tmp = m_modsfound;

            for(auto it=qr.mods.begin(); it!=qr.mods.end(); it++) {
                wxString name;
                wxString hash;

                name << it->name;
                hash << it->hash;

                wxListItem item;
                item.SetId(0);
                item.SetText(name);
                long tmp=m_modlist->InsertItem(item);

                item.SetId(tmp);
                item.SetColumn(1);
                item.SetText(hash);
                m_modlist->SetItem(item);

                bool found =false;
                auto it2=mods_tmp.begin();
                while(it2!=mods_tmp.end() && !found) {
                    if(it2->name == it->name) {
                        found = true;
                    } else
                        it2++;
                }
                if(found) {
                    item.SetId(tmp);
                    item.SetColumn(2);
                    item.SetText("V");
                    m_modlist->SetItem(item);
                }



            }
            wxApp::GetInstance()->Yield();
        };
        gs->getServerDetails(rules_callbacks);


        std::stringstream ss;
        ss << "-connect=" << gs->getIpAddress();
        ss << " -port=" << gs->getPort();
        serverconnect = ss.str();
        updateCmd();
    }

    void onEnableHyperThreading( wxCommandEvent& event ) {
        settings.parameters.enable_hyperthreading = m_enable_hyperthreading->IsChecked();
    }
    void onNoSplash( wxCommandEvent& event ) {
        settings.parameters.no_splash = m_no_splash->IsChecked();
    }
    void onSkipIntro( wxCommandEvent& event ) {
        settings.parameters.skip_intro = m_skip_intro->IsChecked();
    }
    void onWindowed( wxCommandEvent& event ) {
        settings.parameters.windowed = m_windowed->IsChecked();
    }
    void onCores( wxSpinEvent& event ) {
        settings.parameters.cores = m_cores->GetValue();
    }
    void onThreads( wxSpinEvent& event ) {
        settings.parameters.threads = m_threads->GetValue();
    }
    void onAdditionalParameters( wxCommandEvent& event ) {
        settings.parameters.additional_parameters = m_additional_parameters->GetValue().ToUTF8();
    }

    void onVBlank( wxCommandEvent& event ) {
        settings.parameters.vblank = !m_vblank->IsChecked();
    }




    void onItemActivated( wxListEvent& event ) {

    }


    void onItemDeselected( wxListEvent& event ) {
        serverconnect.clear();
        updateCmd();
    }

    void updateCmd() {
        m_statusBar1->SetStatusText(createLaunchCmd());
    }

    void setup() {
        Connect(wxID_ANY, wxEVT_IDLE, wxIdleEventHandler(MyMainFrame::onIdle) );
    }

    std::function<void(int, bool)> m_toggle_cb;

    std::string createLaunchCmd() {
        std::ostringstream ss;
        ss << "./arma3 " << serverconnect;
        if(modsquery)
            ss << " " << modsquery->getCommand();
        return ss.str();
    }

    void startGame(const std::string &params) {
        std::ostringstream ss;

        ss << "./arma3 " << params;
        ss << settings.generateParametersString();

        if(modsquery) {
            ss << modsquery->getCommand();
            try {
                boost::system::error_code e;
                boost::filesystem::path folder(settings.game_path);
                folder /="a3browser";
                boost::filesystem::create_directory(folder, e);
                auto it=modsquery->getFoundMods().begin();
                while(it!=modsquery->getFoundMods().end()) {
                    if(it->checked) {
                        boost::filesystem::create_symlink(it->location, folder/boost::filesystem::basename(it->location), e);
                    }
                    it++;
                }
            } catch(...) {
            }

        }

      chdir(m_dir_arma3->GetPath().ToAscii());

        system(ss.str().c_str());
    }

    void onLaunchClicked(wxCommandEvent& event) {
        startGame("");
    }

    void onJoinClicked(wxCommandEvent& event) {
        startGame(serverconnect);
    }

    void scanMods() {
        m_toggle_cb = [this](int idx, bool checked) {
            modsquery->setChecked(idx, checked);
        };
        ModsReceivedCallback cb = [this](Mods &mods) {
            m_modsfound = mods;
            setMods(mods, m_toggle_cb);
        };

        if(!modsquery)
            modsquery = steam.getInstalledMods(settings.mod_path);
        modsquery->getMods(cb);
    }


    std::function<void(int, bool)> m_clickEvent_cb;
    void setMods(const Mods &mods, std::function<void(int, bool)> clickEvent_cb) {
        m_clickEvent_cb = clickEvent_cb;
        std::vector<wxString> strings;
        std::vector<int> selected;
        int idx=0;
        for(auto it(mods.begin()); it!=mods.end(); it++ ) {
            strings.push_back(wxString() << it->name);
            if(it->checked)
                selected.push_back(idx);
            idx++;
        }
        if(!strings.empty())
            m_checkedmodlist->InsertItems(strings.size(), strings.data(),0);
        for(auto it(selected.begin()); it!=selected.end(); it++)
            m_checkedmodlist->Check(*it, true);
    }

    void onModToggled( wxCommandEvent& event ) {
        if(m_clickEvent_cb)
            m_clickEvent_cb(event.GetInt(), event.IsChecked());
    }

    Mods m_modsfound;
    ModsQuery_ptr modsquery=nullptr;
    ServerListResponse_ptr server_response;
    CallbackTicker *ticker=nullptr;
    ISteamMatchmakingServerListResponseCallback callbacks;
    ISteamMatchmakingRulesResponseCallback rules_callbacks;
    wxString command;
    std::string serverconnect;
    SteamConnection steam;
};



ArmaLauncher::ArmaLauncher() {
}


ArmaLauncher::~ArmaLauncher() {
}

bool ArmaLauncher::OnInit() {
    wxInitAllImageHandlers();
    frame = new MyMainFrame();
    ((MyMainFrame*)frame)->setup();
    frame->Show(true);
    return true;
}

wxIMPLEMENT_APP(ArmaLauncher);
