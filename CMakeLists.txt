cmake_minimum_required(VERSION 3.1)
set(CMAKE_CXX_STANDARD 11)
project (a3launcher)

add_subdirectory(lib)
include_directories(lib ${wxWidgets_INCLUDE_DIRS})

add_subdirectory(wx-gui)
add_subdirectory(qt-gui)

